def fib_interval(x):
    """
    liefert ein Tupel mit der grössten Fibonacci-Zahl, kleiner oder
    gleich x, und der kleinsten Fibonacci-Zahl, grösser oder gleich x, zurück
    """

    if x < 0:
        return -1

    old, new = 0, 1

    while True:
        if new < x:
            old, new = new, old + new
        else:
            return old, new


for i in range(4, 15):
    print(i, fib_interval(i))
