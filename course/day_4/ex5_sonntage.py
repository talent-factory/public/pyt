def sonntage_auf_erstem():
    letzter_tag_im_vormonat = int(input("Bitte den letzten Tag des Vorjahres eingeben (1-7): "))
    schaltjahr = input("Ist das aktuelle Jahr ein Schaltjahr? (ja/nein) ")

    # Anzahl der Tage in jedem Monat (für ein normales Jahr)
    tage_pro_monat = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    monatsnamen = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober",
                   "November", "Dezember"]

    # Ändere Februar auf 29 Tage, wenn es ein Schaltjahr ist
    if schaltjahr.lower() == "ja":
        tage_pro_monat[1] = 29

    for i in range(12):  # Für jeden Monat in einem Jahr
        tag = (letzter_tag_im_vormonat + 1) % 7  # Der erste Tag des aktuellen Monats
        letzter_tag_im_vormonat = (letzter_tag_im_vormonat + tage_pro_monat[i]) % 7  # Der letzte Tag des aktuellen Monats

        # Wenn der erste Tag des Monats ein Sonntag ist (7 in unserer Eingabe), gib das Datum aus
        if tag == 0:
            print("1. " + monatsnamen[i])


# Aufruf der Funktion
sonntage_auf_erstem()
