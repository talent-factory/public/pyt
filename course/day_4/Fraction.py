class Fraction(object):

    def __init__(self, numerator, denominator):
        self.__numerator, self.__denominator = self.shorten(numerator, denominator)

    def __str__(self):
        return f'{self.__numerator} / {self.__denominator}'

    @staticmethod
    def gcd(a, b):
        """Greatest common divisor (GCD) - Grösste gemeinsame Teiler (ggT)"""
        while b != 0:
            a, b = b, a % b
        return a

    @classmethod
    def shorten(cls, numerator, denominator):
        gcd = cls.gcd(numerator, denominator)
        return numerator // gcd, denominator // gcd


if __name__ == '__main__':
    x = Fraction(3528, 3780)
    print(x)
