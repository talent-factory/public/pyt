def sum_n(n):
    if n == 0:
        return 0
    else:
        return n + sum_n(n-1)


def sum_2(n):
    result = 0
    while n > 0:
        result += n
        n -= 1
    return result


print("Die Summe der Zahlen von 1 bis 100: " + str(sum_2(100)))
