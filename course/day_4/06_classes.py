# noinspection PyMethodMayBeStatic
class Person:

    def say_hello(self):
        print("Hello")


if __name__ == '__main__':

    x = 5
    p = Person()
    # marie = Person()
    #
    # marie.say_hello()
    # print(peter == marie)

    print(type(x))
    print(type(p))
