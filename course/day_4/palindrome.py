def is_palindrome_1(name):
    """Berechnen mit einer Schleife"""
    for i in range(len(name) // 2):
        if name[i] != name[-(i + 1)]:
            return False
    return True


def is_palindrome_2(name):
    """Rekursive Berechnung"""
    if len(name) <= 1:
        return True
    elif name[0] != name[-1]:
        return False
    else:
        return is_palindrome_2(name[1:-1])


def is_palindrome_3(name):
    """Direkter Listenvergleich"""
    return list(name) == list(reversed(name))


def is_palindrome_4(name):
    """Slicing und Umkehrung"""
    return name == name[::-1]


def is_palindrome_5(name):
    """List Comprehension"""
    return all(name[i] == name[-(i + 1)] for i in range(len(name) // 2))


for value in ('Daniel', 'Anna', 'Peter', 'Reittier'):
    value = value.lower()
    print(value, is_palindrome_1(value))
    print(value, is_palindrome_2(value))
    print(value, is_palindrome_3(value))
    print(value, is_palindrome_4(value))
    print(value, is_palindrome_5(value))

