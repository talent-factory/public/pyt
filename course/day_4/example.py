def calculate_grains(num_fields: int):
    total_grains = 0
    current_grains = 1

    for _ in range(num_fields):
        total_grains += current_grains
        current_grains *= 2

    return total_grains


def main():
    # Beispielaufruf des Algorithmus mit 3 Feldern
    fields = int(input("Anzahl Feldern: "))
    result = calculate_grains(fields)
    print("Die Gesamtanzahl der Körner auf", fields, "Feldern beträgt:", result)


if __name__ == "__main__":
    main()
