"""Mit diesem Beispiel wollen wir die Funktionalität
von Ausnahmen (Exceptions) zeigen."""

# Was passiert bei einer Division durch '0'

x, y = 3, 0
print(x / y)

# Hier ein anderes Beispiel
while True:
    zahl = input('Bitte eine Zahl eingeben: ')
    print(type(zahl))
    zahl = int(zahl)

# Besser...
while True:
    try:
        zahl = input('Bitte eine Zahl eingeben: ')
        zahl = int(zahl)
    except ValueError as e:
        print(f'Fehler: {e}')
    finally:
        print(f'Finally..')


# Spezielle Variable `nan`
# noinspection PyUnreachableCode
from math import nan

x, y = 1, nan
print(x / y)

x = [1, 2, 3]

print(x)


class Roboter:

    def say_hello(self):
        print("Hello")
