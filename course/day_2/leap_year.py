year = int(input("Gib ein Jahr ein: "))

# Formel gemäss https://www.umrechnung.org/zeit-datum-kalender/schaltjahr.htm
if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
    is_leap_year = True
else:
    is_leap_year = False

if is_leap_year:
    print(f"{year} ist ein Schaltjahr.")
else:
    print(f"{year} ist kein Schaltjahr.")
