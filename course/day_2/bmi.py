# BMI Rechner

print('Berechnen des BMI')

weight = float(input('Gewicht [kg]: '))
size = float(input('Höhe [m]    : '))

bmi = weight / (size ** 2)

print(f'BMI: {bmi:.2f} : Gewicht: {weight}, Höhe: {size}')
print('BMI: %.2f : Gewicht: %.1f, Höhe: %.1f' % (bmi, weight, size))
