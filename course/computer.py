# Pynoinspection MethodMayBeStatic
# noinspection PyMethodMayBeStatic
class Computer:

    def __init__(self, manufacturer: str, year: int):
        self.__manufacturer = manufacturer
        self.year = year

    @property
    def year(self):
        return self.__year

    @year.setter
    def year(self, year: int):
        if year > 1999:
            self.__year = year
        else:
            raise ValueError('Invalid year')

    def __str__(self):
        return f"{self.__manufacturer} - {self.__year}"


var1 = Computer('Apple', 2021)
var2 = Computer('Samsung', 2023)

var2.year = var1.year

print(var1)
print(var2)
