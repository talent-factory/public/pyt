total = 0

old_sum = -1
eps = 0.00001
i = 1

while total - old_sum > eps:
    old_sum = total
    total += i
    i /= 2
    print(i, total)
