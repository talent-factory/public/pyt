n = 100
total = 0

i = 1
while i <= n:
    total += i
    i += 1

print(f'Summe von 1 bis {n}: {total}')
