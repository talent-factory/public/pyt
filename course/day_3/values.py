#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Mit diesem Script lesen wir eine Liste von Zahlen von der
Tastatur ein, Anschliessend verarbeiten wir diese Liste und
entfernen die Duplikate.

Beispiel:
eingabe: [1, 1, 2, 3]
ausgabe: [1, 2, 3]
"""
my_list = eval(input('Liste mit positiven Zahlen: '))

n = len(my_list)
i = 0
current = 0
previous = None

result = []

while i < n:
    current = my_list[i]
    i = i + 1
    if current == previous:
        continue
    if current <= 0:
        print('Abbruch')
        break
    result.append(current)
    previous = current

print(result)
