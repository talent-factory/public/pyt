class Person:

    def __init__(self, name, first_name):
        self.__name = name
        self.__first_name = first_name

    def __str__(self):
        return f'{self.__name}, {self.__first_name}'


if __name__ == '__main__':
    person = Person('Senften', 'Daniel')

    print(person)
