def fib(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a


def fiblist(n):
    my_list = [0, 1]
    for i in range(1, n):
        my_list += [my_list[-1] + my_list[-2]]
    return my_list
