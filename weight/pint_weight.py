import pint

length = pint.UnitRegistry()

l1 = 3 * length.meter
l2 = 2 * length.cm

l3 = l2 + l1

print(l3, type(l3))
