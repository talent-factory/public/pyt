def is_palindrome(value):
    value = str(value)
    return value == value[::-1]


x = "Anna".lower()

print(f'{x} is a palindrome? {is_palindrome(x)}')
