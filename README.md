# Einführung in Python 

## Installation

- Aufgrund der `numpy` Module sind alle Beispiele mit Python v3.7 getestet
- Installieren aller Abhängigkeiten: `pip install -r requirements.txt`
- Installation der Module aus dem Kurs der [Princeton University](https://introcs.cs.princeton.edu/python/home/). 
  `cd introcs-1.0; python setup.py install`


## Dokumentation

---

Die im Anhang D ([Python Programming Cheatsheet](https://introcs.cs.princeton.edu/python/appendix_cheatsheet/)) 
vorgestelle Zusammenfassung ist 
als PDF Dokument [hier](https://ipfs.filebase.io/ipfs/QmXnGesnuAByPmxmaZpbgmML4MZYxesdhjXKLSmPzCjcNS) abgelegt.

Die verwendeten Folien des Kurses finden sie im Verzeichnis [doc](doc).