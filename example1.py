language = 'fr'

menu_de = {
        'File': 'Datei',
        'New': 'Neu',
        'Open': 'Öffnen',
        'Save': 'Speichern',
        'Save As': 'Speichern als',
        'Exit': 'Beenden'
    }

menu_fr = {
        'File': 'Fichier',
        'New': 'Nouveau',
        'Open': 'Ouvrir',
        'Save': 'Enregistrer',
        'Save As': 'Enregistrer sous',
        'Exit': 'Exit'
    }

menu_en = {
        'File': 'File',
        'New': 'New',
        'Open': 'Open',
        'Save': 'Save',
        'Save As': 'Save As',
        'Exit': 'Exit'
    }

menu = {
    'de': menu_de,
    'en': menu_en,
    'fr': menu_fr
}


print(menu[language]['Save As'])
