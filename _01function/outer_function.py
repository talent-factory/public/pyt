def outer_function():
    x = 10  # Lokale Variable in der äußeren Funktion

    def inner_function():
        nonlocal x  # Zugriff auf die äußere Variable
        x += 5
        print("In inner_function:", x)

    inner_function()
    print("In outer_function:", x)


outer_function()
