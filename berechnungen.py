import fibonacci.functions as f
from fibonacci.functions import fib

x = 10
print(f'fib({x}): {f.fib(x)}')
print(f'fib({x}): {fib(x)}')
