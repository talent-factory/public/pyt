from decimal import Decimal


class Currency:
    """List of all currencies we support.
    When normalizing currencies, CHF is always used for internal calculations
    """

    CHF = None
    EUR = None
    USD = None
    GBP = None

    def __init__(self, currency_name, currency_rate, decimal_precision, sub_unit):
        self.__currency_name = currency_name
        self.__currency_rate = currency_rate
        self.__decimal_precision = decimal_precision
        self.__sub_unit = sub_unit

    def __str__(self):
        return self.__currency_name

    @property
    def name(self):
        return self.__currency_name

    @property
    def currency_rate(self) -> Decimal:
        return Decimal(self.__currency_rate)

    @property
    def decimal_precision(self) -> int:
        return self.__decimal_precision

    @property
    def sub_unit(self) -> int:
        return self.__sub_unit


# Zuweisung der Währungen
Currency.EUR = Currency("EUR", 1.10, 2, 100)
Currency.CHF = Currency("CHF", 1.00, 2, 100)
Currency.USD = Currency("USD", 0.93, 2, 100)
Currency.GBP = Currency("GBP", 1.29, 2, 100)
